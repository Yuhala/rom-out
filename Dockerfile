#
# Author: Peterson Yuhala
# Docker file for untrusted romulus container: this container memory maps the file to its address space
#

FROM sconecuratedimages/crosscompilers

RUN  apk update \
    && apk add --no-cache --virtual=.builddeps \
        bash \
        build-base \
        make \
        musl-dev \
        openjdk8 \
        wget \
        python3 \
        python3-dev \
        zip \
        libc6-compat \
        libexecinfo \
        libunwind \
        libexecinfo-dev \
        libunwind-dev \
        patch \
        perl \
        sed \
        git \
        libc-dev \
        gcc \
        g++ \
        linux-headers \
        ca-certificates \
        nss \
        && git clone https://gitlab.com/Yuhala/rom-out.git
    
#git clone https://gitlab.com/Yuhala/rom-out.git
RUN cd /rom-out && g++ -O3 -o rom-out rom-out.cpp


CMD bash -c "SCONE_VERSION=1 /rom-out/rom-out"


